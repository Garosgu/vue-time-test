import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Vuex from 'vuex'
import BootstrapVue from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import { i18n } from './translate/translate'
import { store } from './store/store'

Vue.use(VueAxios, axios)
Vue.use(Vuex)
Vue.use(BootstrapVue)

new Vue({
  el: '#app',
  store,
  i18n,
  render: h => h(App)
})
